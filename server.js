/**
 * http://usejsdoc.org/
 */

var http = require('http');
var socket = require('socket.io');
var fs = require('fs');

var mysql = require('mysql');

var connection = mysql.createConnection({
	host : 'localhost',
	user : 'root',
	database : 'test',
	password : 'Wasure71'
});

connection.connect();

var server = http.createServer(function(req, res){
	res.writeHead(200, {'Content-Type' : 'text/html'});
	res.end(fs.readFileSync(__dirname + '/views/index.html', 'utf-8'));
}).listen(3000);

var io = socket.listen(server);

var chat = io.of('/chat').on('connection', function(socket){
/*io.sockets.on('connection', function(socket){*/
	var name;
	var room;

	socket.on('client_to_server_leave', function(data){
		var leaveMsg = data.name + "さんはグループから退出しました";
		var id = socket.id;
		socket.broadcast.to(room).emit('server_to_client', {value : leaveMsg});
		chat.to(id).emit('server_to_client', {value : 'あなたはグループから退出しました'});
		socket.leave(room);
	});

	socket.on('client_to_server_join', function(data){
		room = data.selectRoom;
		socket.join(room);
		var sql = 'select * from chats where group_id = '+ room + ' order by created_date asc limit 15;';
		connection.query(sql, function(err, results, fields){
			if (err) { console.log('err: ' + err); }
			chat.to(socket.id).emit('server_to_client_all', {value : results});
		});
	});
	socket.on('client_to_server', function(data){
		var message = data.message;
		var name = data.name;
		var selectRoom = data.selectRoom;
		var sql = 'insert into chats(name, text, group_id)values("'+name+'","'+message+'","'+selectRoom+'");';
		connection.query(sql, function(err, result){
			if (err) { console.log('err: ' + err); }
		});
		var newMsg =  "[" + name + "]: " + message;
		chat.to(room).emit('server_to_client', {value : newMsg});
	});
	 socket.on('client_to_server_broadcast', function(data) {
		 var entryMessage = data.name + "さんが入室しました。";
        socket.broadcast.to(room).emit('server_to_client', {value : entryMessage});
    });
	 socket.on('client_to_server_personal', function(data){
		 var id = socket.id;
		 name = data.name;
		 var personalMessage = "あなたは" + name + "さんとして入室しました";
		 chat.to(id).emit('server_to_client', {value : personalMessage});
	 });
	 socket.on('disconnect', function(){
		 var endMessage;
		 if(name === undefined){
			 endMessage = '名前未入力のまま退出しました';
			/* io.sockets.emit('server_to_client', {value : endMessage});*/
			chat.emit('server_to_client', {value : endMessage});
		 }else{
			 endMessage = name + 'さんが退出しました';
			 chat.to(room).emit('server_to_client', {value : endMessage});
		 }
	 });

});

var fortune = io.of('/fortune').on('connection', function(socket){
	socket.on('show_fortune', function(){
		var id = socket.id;
		var fortunes = ["大吉", "吉", "中吉", "小吉", "末吉", "凶", "大凶"];
		var selectedFortune = fortunes[Math.floor(Math.random()*fortunes.length)];
		var todaysFortune = "あなたの今日の運勢は..." + selectedFortune + "です。";
		fortune.to(id).emit('server_to_client', {value : todaysFortune});
	});
});